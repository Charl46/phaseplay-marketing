<?php
/**
 * Firebase Service
 */

defined('ABSPATH') || exit;

if (!class_exists('FirebaseService', false)):

    /**
     * FirebaseService Class
     */
    class FirebaseService {
        protected $firebase_settings = null;
        protected $options_wordpress = null;

        public function __construct() {
            $this->firebase_settings = get_option('firebase_settings');
            $this->options_wordpress = get_option("firebase_wordpress");
        }

        public function get_firebase_setting() {
            return $this->firebase_settings;
        }

        private function collection_name_generetor($post_type) {
            $type_object = get_post_type_object($post_type);

            $plural_name = $type_object->labels->name;
            $name_array = preg_split("/[_,\- ]+/", $plural_name);
            $name_array = array_map('ucfirst', $name_array);

            return 'wp' . implode("", $name_array);
        }

        public function save_wordpress_data_to_firebase($post_id, $post) {
            if (
            is_array($this->options_wordpress['wp_sync_post_types'])
            || isset($this->options_wordpress['wp_sync_custom_post_types'])
        ) {
                $base_domain = $this->firebase_settings['base_domain'];
                $api_token = $this->firebase_settings['dashboard_api_token'];

                $collection_name = null;

                $post_type = $post->post_type;

                if (
                in_array($post_type, $this->options_wordpress['wp_sync_post_types'])
                || strpos($this->options_wordpress['wp_sync_custom_post_types'], $post_type) !== false
            ) {
                    $collection_name = $this->collection_name_generetor($post_type);
                }

                if ($collection_name) {

                    $url = $base_domain . "/api-database/v1/createDoc";
                    $data = new stdClass();
                    $data->dbType = $this->options_wordpress['wp_sync_database_type'];
                    $data->collection = $collection_name;
                    $data->docId = (string) $post_id;
                    $data->data = $post;

                    $response = wp_remote_post($url, array(
                        'method' => 'POST',
                        'headers' => array(
                            'api-token' => $api_token,
                            'source' => 'dashboard',
                            'Content-Type' => 'application/json; charset=utf-8',
                        ),
                        'body' => json_encode($data),
                    ));

                    if (!is_wp_error($response)) {
                        $result = json_decode($response['body']);
                        if ($result->status) {
                            return $result;
                        } else {
                            error_log($result->message);
                        }
                    }

                } else {
                    error_log('Integrate Firebase PRO does not support post type: ' . $post_type);
                }
            }
        }

        public function sync_user_to_firebase($firebase_user) {
            if (!empty($this->options_wordpress['wp_sync_users'])) {

                unset($firebase_user['password']);
                $filtered_data = array_filter($firebase_user);

                // Prepare data for updating
                $base_domain = $this->firebase_settings['base_domain'];
                $api_token = $this->firebase_settings['dashboard_api_token'];

                $url = $base_domain . "/api-database/v1/updateDoc";
                $data = new stdClass();
                $data->dbType = $this->options_wordpress['wp_sync_database_type'];
                $data->collection = $this->options_wordpress['wp_sync_users'];
                $data->docId = (string) $filtered_data['userId'];
                $data->data = $filtered_data;

                $response = wp_remote_post($url, array(
                    'method' => 'POST',
                    'headers' => array(
                        'api-token' => $api_token,
                        'source' => 'dashboard',
                        'Content-Type' => 'application/json; charset=utf-8',
                    ),
                    'body' => json_encode($data),
                ));

                if (!is_wp_error($response)) {
                    $result = json_decode($response['body']);
                    if ($result->status) {
                        return $result;
                    } else {
                        error_log($result->message);
                    }
                }
            }
        }
    }

endif;

return new FirebaseService();
