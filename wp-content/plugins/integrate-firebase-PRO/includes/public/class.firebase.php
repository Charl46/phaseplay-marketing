<?php

class Firebase {
    private static $initiated = false;
    private static $options;
    private static $options_auth;
    private static $options_settings;
    private static $options_wordpress;

    public static function init() {
        if (!self::$initiated) {
            self::init_hooks();
        }
    }

    public static function init_hooks() {
        self::$initiated = true;
        self::$options = get_option("firebase_credentials");
        self::$options_auth = get_option("firebase_auth");
        self::$options_settings = get_option("firebase_settings");
        self::$options_wordpress = get_option("firebase_wordpress");

        // Redirect Login Page when Login With Firebase is checked
        if (self::$options_auth && isset(self::$options_auth['login_with_firebase'])) {
            add_action('init', array('Firebase', 'prevent_wp_login'));
        }
        add_action('wp_enqueue_scripts', array('Firebase', 'load_firebase_js'));

        // WP API
        $rest_api_users = new Firebase_Rest_Api_User();
        $rest_api_users->init();

        // Add-on initialization
        if (class_exists('Firebase_Maps')) {
            Firebase_Maps::init();
        }
    }

    public static function prevent_wp_login() {
        global $pagenow;
        if ('wp-login.php' == $pagenow && !is_user_logged_in()) {
            // Redirect to default login page or to dedicated page for login
            if (self::$options_auth['login_url']) {
                $redirect_url = self::$options_auth['login_url'];
                wp_redirect($redirect_url);
                exit();
            }
        }
    }

    public static function load_firebase_js() {

        // @TODO will be imported from Webpack
        // wp_enqueue_style( 'firebase', plugin_dir_url( dirname(__FILE__) ) . 'css/firebase.css' );

        $FIREBASE_SERVICES = isset(self::$options['firebase_services']) ? self::$options['firebase_services'] : array();
        // Set language for FirebaseUI Web
        $FIREBASE_LANGUAGE_LIST = require_once FIREBASE_WP__PLUGIN_DIR . 'i18n/firebaseui_web.php';

        $current_wp_langague = "en";
        if (isset($FIREBASE_LANGUAGE_LIST[get_locale()])) {
            $current_wp_langague = $FIREBASE_LANGUAGE_LIST[get_locale()];
        }

        wp_enqueue_script('firebase_app', 'https://www.gstatic.com/firebasejs/7.21.0/firebase-app.js', array(), FIREBASE_WP_VERSION, true);
        wp_enqueue_script('firebase_auth', 'https://www.gstatic.com/firebasejs/7.21.0/firebase-auth.js', array(), FIREBASE_WP_VERSION, true);

        // Right to left support
        if (in_array($current_wp_langague, ["ar", "fa", "iw"])) {
            wp_enqueue_style('firebase_ui', 'https://www.gstatic.com/firebasejs/ui/4.6.1/firebase-ui-auth-rtl.css', array(), FIREBASE_WP_VERSION, false);
        } else {
            wp_enqueue_style('firebase_ui', 'https://www.gstatic.com/firebasejs/ui/4.6.1/firebase-ui-auth.css', array(), FIREBASE_WP_VERSION, false);
        }

        wp_enqueue_script('firebase_ui', "https://www.gstatic.com/firebasejs/ui/4.6.1/firebase-ui-auth__$current_wp_langague.js", array(), FIREBASE_WP_VERSION, true);

        if (in_array('realtime', $FIREBASE_SERVICES)) {
            wp_enqueue_script('firebase_database', 'https://www.gstatic.com/firebasejs/7.21.0/firebase-database.js', array(), FIREBASE_WP_VERSION, true);
        }

        if (in_array('firestore', $FIREBASE_SERVICES)) {
            wp_enqueue_script('firebase_firestore', 'https://www.gstatic.com/firebasejs/7.21.0/firebase-firestore.js', array(), FIREBASE_WP_VERSION, true);
        }

        if (in_array('storage', $FIREBASE_SERVICES)) {
            wp_enqueue_script('firebase_storage', 'https://www.gstatic.com/firebasejs/7.21.0/firebase-storage.js', array(), FIREBASE_WP_VERSION, true);
        }

        if (in_array('analytics', $FIREBASE_SERVICES)) {
            wp_enqueue_script('firebase_analytics', 'https://www.gstatic.com/firebasejs/7.21.0/firebase-analytics.js', array(), FIREBASE_WP_VERSION, true);
        }

        // Datatables Assets
        wp_enqueue_style('firebase-datatables', '//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css', array('jquery'), FIREBASE_WP_VERSION, false);
        wp_enqueue_script('firebase-datatables', '//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js', array('jquery'), FIREBASE_WP_VERSION, true);

        wp_enqueue_script('firebase', FIREBASE_WP__PLUGIN_URL . 'js/firebase.js', array('jquery', 'firebase_app', 'firebase_ui', 'firebase_auth'), FIREBASE_WP_VERSION, true);

        wp_localize_script(
            'firebase',
            'firebaseOptions', array(
                'apiKey' => isset(self::$options['api_key']) ? self::$options['api_key'] : '',
                'authDomain' => isset(self::$options['auth_domain']) ? self::$options['auth_domain'] : '',
                'databaseURL' => isset(self::$options['database_url']) ? self::$options['database_url'] : '',
                'storageBucket' => isset(self::$options['storage_bucket']) ? self::$options['storage_bucket'] : '',
                'appId' => isset(self::$options['app_id']) ? self::$options['app_id'] : '',
                'measurementId' => isset(self::$options['measurement_id']) ? self::$options['measurement_id'] : '',
                'projectId' => isset(self::$options['project_id']) ? self::$options['project_id'] : '',
                'services' => $FIREBASE_SERVICES,
            )
        );

        wp_localize_script(
            'firebase',
            'authSettings',
            array(
                'loginWithFirebase' => isset(self::$options_auth['login_with_firebase']) ? self::$options_auth['login_with_firebase'] : null,
                'loginUrl' => isset(self::$options_auth['login_url']) ? self::$options_auth['login_url'] : null,
                'signinWithEmailLink' => isset(self::$options_auth['signin_with_email_link']) ? self::$options_auth['signin_with_email_link'] : null,
                'googleClientId' => isset(self::$options_auth['google_client_id']) ? self::$options_auth['google_client_id'] : null,

                'signInSuccessUrl' => isset(self::$options_auth['sign_in_success_url']) ? self::$options_auth['sign_in_success_url'] : null,
                'signInOptions' => isset(self::$options_auth['sign_in_options']) ? self::$options_auth['sign_in_options'] : null,
                'tosUrl' => isset(self::$options_auth['tos_url']) ? self::$options_auth['tos_url'] : null,
                'privacyPolicyUrl' => isset(self::$options_auth['privacy_policy_url']) ? self::$options_auth['privacy_policy_url'] : null,
            )
        );

        wp_localize_script(
            'firebase',
            'firebaseSettings',
            array(
                'baseDomain' => isset(self::$options_settings['base_domain']) ? self::$options_settings['base_domain'] : null,
                'frontendApiToken' => isset(self::$options_settings['frontend_api_token']) ? self::$options_settings['frontend_api_token'] : null,
            )
        );

        wp_localize_script(
            'firebase',
            'firebaseWordpress',
            array(
                'siteUrl' => get_site_url(),
                'userCollectionName' => isset(self::$options_wordpress['wp_sync_users']) ? self::$options_wordpress['wp_sync_users'] : null,
            )
        );

        $public_translations = require_once FIREBASE_WP__PLUGIN_DIR . 'i18n/public_translations.php';
        wp_localize_script('firebase', 'firebaseTranslations', $public_translations);
    }
}
?>
