<?php

/**
 * REST API for User Management
 */

class Firebase_Rest_Api_User {
    private static $initiated = false;
    private static $firebase_service;
    private static $options_settings;

    public static function init() {
        if (!self::$initiated) {
            self::init_hooks();
        }
    }

    public static function init_hooks() {
        self::$initiated = true;
        self::$options_settings = get_option("firebase_settings");
        // Import Firebase Service
        if (isset(self::$options_settings['base_domain']) && isset(self::$options_settings['dashboard_api_token'])) {
            self::$firebase_service = include FIREBASE_WP__PLUGIN_DIR . 'includes/service/class.firebase-service.php';
        }
        add_action('rest_api_init', array('Firebase_Rest_Api_User', 'add_api_routes'));
    }

    public static function add_api_routes() {
        /**
         * User API Paths
         */
        register_rest_route('firebase/v2', 'users/register', array(
            'methods' => 'POST',
            'callback' => array('Firebase_Rest_Api_User', 'register_user'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route('firebase/v2', 'users/register-autologin', array(
            'methods' => 'POST',
            'callback' => array('Firebase_Rest_Api_User', 'register_and_autologin'),
            'permission_callback' => '__return_true',
        ));
    }

    private static function update_user_data($user, $firebase_user) {
        update_usermeta($user->ID, "firebase_uid", $firebase_user['userId']);

        $first_name = get_user_meta($user->ID, 'first_name', true);

        // Prevent update user data if they set it manually in WordPress
        if (empty($first_name)) {
            // 1 - Assign display name to phone number
            // 2- Then assign display name to firbase user displayname

            if (!empty($firebase_user['phoneNumber'])) {
                wp_update_user(array('ID' => $user->ID, 'display_name' => $firebase_user['phoneNumber']));
                wp_update_user(array('ID' => $user->ID, 'nickname' => $firebase_user['phoneNumber']));
            }

            if (!empty($firebase_user['displayName'])) {
                wp_update_user(array('ID' => $user->ID, 'display_name' => $firebase_user['displayName']));
                wp_update_user(array('ID' => $user->ID, 'nickname' => $firebase_user['displayName']));
            }
            if (!empty($firebase_user['firstName'])) {
                update_usermeta($user->ID, "first_name", $firebase_user['firstName']);
            }
            if (!empty($firebase_user['lastName'])) {
                update_usermeta($user->ID, "last_name", $firebase_user['lastName']);
            }
        }
    }

    public function register_user(WP_REST_Request $request = null) {

        $response = array();
        $parameters = $request->get_json_params();

        $username = sanitize_text_field($parameters['username']);
        $email = sanitize_text_field($parameters['email']);
        $password = sanitize_text_field($parameters['password']);

        $error = new WP_Error();

        if (empty($username)) {
            $error->add(400, __("Username field 'username' is required.", 'integrate-firebase-PRO'), array('status' => 400));
            return $error;
        }
        if (empty($email)) {
            $error->add(401, __("Email field 'email' is required.", 'integrate-firebase-PRO'), array('status' => 400));
            return $error;
        }
        if (empty($password)) {
            $error->add(404, __("Password field 'password' is required.", 'integrate-firebase-PRO'), array('status' => 400));
            return $error;
        }

        // Check if user exits
        $user_id = username_exists($username);
        if (!$user_id && email_exists($email) == false) {
            $user_id = wp_create_user($username, $password, $email);
            if (!is_wp_error($user_id)) {
                $user = get_user_by('id', $user_id);
                $user->set_role('subscriber');
                // WooCommerce role
                if (class_exists('WooCommerce')) {
                    $user->set_role('customer');
                }
                do_action('wp_rest_user_user_register', $user);

                $response['code'] = 200;
                $response['message'] = __("User is created: " . $username, "integrate-firebase-PRO");
            } else {
                return $user_id;
            }
        } else if ($user_id) {
            $error->add(400, __("Username already exists, please enter another username", 'integrate-firebase-PRO'), array('status' => false));
            return $error;
        } else {
            $error->add(400, __("Email already exists, please try 'Reset Password'", 'integrate-firebase-PRO'), array('status' => false));
            return $error;
        }

        return new WP_REST_Response($response, 200);
    }

    public static function register_and_autologin(WP_REST_Request $request = null) {
        $response = array();
        $parameters = $request->get_json_params();

        $error = new WP_Error();

        $request_origin = $request->get_header('origin');
        $sec_fetch_site = $request->get_header('sec_fetch_site');
        $site_url = get_site_url();

        if (!empty($sec_fetch_site) && $sec_fetch_site != 'same-origin') {
            $error->add(400, __("Invalid Sec Fetch Site", 'integrate-firebase-PRO'), array('status' => false));
            return $error;
        }

        if (
            empty($request_origin) || strpos($site_url, $request_origin) === false
        ) {
            $error->add(400, __("Invalid request Origin", 'integrate-firebase-PRO'), array('status' => false));
            return $error;
        }

        $firebase_user = $parameters['user'];

        $username = sanitize_text_field($firebase_user['userId']);
        $password = sanitize_text_field($firebase_user['password']);
        $email = sanitize_text_field($firebase_user['email']);

        if (empty($username)) {
            $error->add(400, __("'userId' is required.", 'integrate-firebase-PRO'), array('status' => false));
            return $error;
        }
        if (empty($password)) {
            $error->add(400, __("'password' is required.", 'integrate-firebase-PRO'), array('status' => false));
            return $error;
        }

        $user = null;

        // check for user with email otherwise check for user from firebase UID
        if (!empty($email)) {
            // Check if user exists with email
            $user = get_user_by('email', $email);
        } else {
            $user = get_user_by('login', $username);
        }

        if (!empty($user)) {
            $username = $user->data->user_login;

            if (is_multisite()) {
                $blog_id = get_current_blog_id();

                // doesn't exist in current blog
                if (!is_user_member_of_blog($user->ID, $blog_id)) {
                    $role = 'subscriber';
                    if (class_exists('WooCommerce')) {
                        $role = 'customer';
                    }
                    add_existing_user_to_blog(array('user_id' => $user->ID, 'role' => $role));
                }
            }
        } else {
            // check for multisite
            $user_id = null;
            if (is_multisite()) {
                $user_id = wpmu_create_user($username, $password, $email);
            } else {
                $user_id = wp_create_user($username, $password, $email);
            }

            if (!is_wp_error($user_id)) {
                $user = get_user_by('id', $user_id);
                // Manually set user role to subscriber for now
                $user->set_role('subscriber');
                // WooCommerce role
                if (class_exists('WooCommerce')) {
                    $user->set_role('customer');
                }
                do_action('wp_rest_user_user_register', $user);
            } else {
                $error->add(400, __("Cannot create a new user with email: $email", 'integrate-firebase-PRO'), array('status' => false));
                return $error;
            }
        }

        // Update user metadata
        self::update_user_data($user, $firebase_user);

        // Start auto signon
        $creds['user_login'] = $username;
        $creds['user_password'] = $password;
        $creds['remember'] = true;

        // Sync User to Firebase
        $firebase_user['wordpressUserId'] = $user->ID;
        self::$firebase_service->sync_user_to_firebase($firebase_user);

        // is_ssl() for https sites
        $autologin_user = wp_signon($creds, is_ssl());

        if ($autologin_user->errors) {
            $error_message = $autologin_user->get_error_message();
            // Dominate WP password from Firebase
            wp_set_password($password, $user->ID);
            $autologin_user = wp_signon($creds, is_ssl());

            if (!$autologin_user->errors) {
                $response['code'] = 200;
                $response['message'] = __("User logged in", "integrate-firebase-PRO");
                return new WP_REST_Response($response, 200);
            } else {
                $response['code'] = 403;
                $response['message'] = __($error_message, "integrate-firebase-PRO");
            }
        } else {
            $response['code'] = 200;
            $response['message'] = __("User logged in", "integrate-firebase-PRO");
            return new WP_REST_Response($response, 200);
        }

        return $response;
    }
}
