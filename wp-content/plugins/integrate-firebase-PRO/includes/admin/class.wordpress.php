<?php

/**
 * WordPress Custom Hooks
 */

class FirebaseCustomWordPress {
    private static $initiated = false;
    private static $options_auth;
    private static $options_wordpress;

    private static $firebase_service;

    public static function init() {
        if (!self::$initiated) {
            self::init_hooks();
        }
    }

    public static function init_hooks() {
        self::$initiated = true;
        self::$options_auth = get_option("firebase_auth");
        self::$options_wordpress = get_option("firebase_wordpress");

        if (self::$options_wordpress && isset(self::$options_wordpress['wp_sync_database_type'])) {
            self::$firebase_service = include FIREBASE_WP__PLUGIN_DIR . 'includes/service/class.firebase-service.php';
            add_action('wp_insert_post', array('FirebaseCustomWordPress', 'listen_to_save_post_event'), 10, 3);
        }

        // Disable password reset for login with firebase feature
        if (self::$options_auth && isset(self::$options_auth['login_with_firebase'])) {
            add_filter('show_password_fields', array('FirebaseCustomWordPress', 'disable_password_fields'));
        }
    }

    public static function disable_password_fields() {
        if (current_user_can('administrator')) {
            return true;
        }
        return false;
    }

    public static function listen_to_save_post_event($post_id, $post, $update) {
        // If this is just a revision, skip action
        if (wp_is_post_revision($post)) {
            return;
        }

        if ($post->post_status == 'publish' || $post->post_status == 'private') {
            $post->permalink = get_the_permalink($post_id);
            $post->post_author_name = get_the_author_meta('display_name', $post->post_author);
            $post->post_thumbnail = get_the_post_thumbnail_url($post_id, 'post-thumbnail');

            // Get post type by post.
            $post_type = $post->post_type;

            // Get post type taxonomies.
            $taxonomies = get_object_taxonomies($post_type, 'objects');

            $taxonomyList = new stdClass();

            foreach ($taxonomies as $taxonomy_slug => $taxonomy) {

                // Get the terms related to post.
                $terms = get_the_terms($post->ID, $taxonomy_slug);

                if (!empty($terms)) {
                    $taxoKey = $taxonomy->label;
                    $taxonomyList->$taxoKey = $terms;

                }
            }

            $post->taxonomies = $taxonomyList;

            // Get custom fields
            $custom_fields = new stdClass();
            $postmetas = get_post_meta($post_id);

            foreach ($postmetas as $key => $value) {
                if (substr($key, 0, 1) !== '_') {
                    if (!empty(get_post_meta($post_id, $key)[0])) {
                        $custom_fields->$key = get_post_meta($post_id, $key)[0];
                    }
                }
            }

            $post->custom_fields = $custom_fields;

            // Get author info
            $author_data = get_userdata($post->post_author)->data;

            $author_object = new stdClass();
            $author_object->id = $author_data->ID;
            $author_object->user_login = $author_data->user_login;
            $author_object->user_nicename = $author_data->user_nicename;
            $author_object->display_name = $author_data->display_name;
            $author_object->user_url = $author_data->user_url;
            $author_object->user_registered = $author_data->user_registered;

            // get user meta data
            $author_metadata = get_user_meta($author_data->ID);

            if ($author_metadata && !empty($author_metadata['firebase_uid'])) {
                $author_object->firebase_uid = $author_metadata['firebase_uid'][0];
            }

            $post->author = $author_object;

            return self::$firebase_service->save_wordpress_data_to_firebase($post_id, $post);
        }
    }
}
