<?php
/**
 * Frontend messsages
 *
 * Returns an array of languages.
 *
 */

defined('ABSPATH') || exit;

return array(
    "auth" => array(
        "emailPasswordMissing" => esc_html__('Your email or password is missing!', 'integrate-firebase-PRO'),
        "enterMissingData" => esc_html__('Please enter missing data!', 'integrate-firebase-PRO'),
        "confirmPassword" => esc_html__('Confirm password is not the same!', 'integrate-firebase-PRO'),
        "invalidPhoneNumber" => esc_html__('Phone number is invalid', 'integrate-firebase-PRO'),
        "invalidForm" => esc_html__('Form is not valid', 'integrate-firebase-PRO'),
        "emailNeededForReset" => esc_html__('Enter your email in order to reset the password.', 'integrate-firebase-PRO'),
        "checkInboxForReset" => esc_html__('Please check your inbox in order to reset your password.', 'integrate-firebase-PRO'),
    ),
    "database" => array(
        "invalidDbType" => esc_html__("'dbType' must be 'firestore' or 'realtime'. Please check your form!", 'integrate-firebase-PRO'),
        "invalidCollectionOrDocument" => esc_html__("Please check your collection and document name in the shortcode!", 'integrate-firebase-PRO'),
        "emptyCollectionOrDocument" => esc_html__("Collection and document name cannot be empty!", 'integrate-firebase-PRO'),
        "invalidCollectionOrDisplayFields" => esc_html__("Please check your collection name and display fields in the shortcode!'", 'integrate-firebase-PRO'),
    ),
    "firebase" => array(
        "firebaseSettingsMissing" => esc_html__("Please enter your Firebase settings!", 'integrate-firebase-PRO'),
    ),
    "woocommerce" => array(
        "loginText" => esc_html__('Please login to check your account.', 'integrate-firebase-PRO'),
    ),
    "utils" => array(
        "greetings" => esc_html__("Greetings", 'integrate-firebase-PRO'),
        "invalidForm" => esc_html__("Form data is invalid", 'integrate-firebase-PRO'),
        "missingData" => esc_html__("Please enter missing data!", 'integrate-firebase-PRO'),
        "confirmPassword" => esc_html__("Confirm password is not the same!", 'integrate-firebase-PRO'),
        "userCreatedSuccessfully" => esc_html__("User is created successfully!", 'integrate-firebase-PRO'),
    ),
);
