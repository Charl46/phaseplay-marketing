<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package phaseplay
 */

get_header();
?>

    <main id="primary" class="site-main single-post">
        <div class="blog-single-action">
            <div class="container">
                <a href="/?page_id=45" class="btn btn-sm btn-outline-gray-300 d-none d-md-inline">
                    <icon class="back-button-icon">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M7.50579 3.2098C7.5723 3.27614 7.62506 3.35494 7.66106 3.4417C7.69706 3.52846 7.71559 3.62147 7.71559 3.71541C7.71559 3.80934 7.69706 3.90235 7.66106 3.98911C7.62506 4.07587 7.5723 4.15467 7.50579 4.22101L3.72519 8.00019L7.50579 11.7794C7.57219 11.8458 7.62486 11.9246 7.66079 12.0113C7.69673 12.0981 7.71522 12.1911 7.71522 12.285C7.71522 12.3789 7.69673 12.4718 7.66079 12.5586C7.62486 12.6454 7.57219 12.7242 7.50579 12.7906C7.43939 12.857 7.36057 12.9096 7.27382 12.9456C7.18707 12.9815 7.09409 13 7.00019 13C6.90629 13 6.81331 12.9815 6.72656 12.9456C6.6398 12.9096 6.56098 12.857 6.49458 12.7906L2.2098 8.50579C2.1433 8.43946 2.09053 8.36065 2.05453 8.27389C2.01853 8.18713 2 8.09412 2 8.00019C2 7.90625 2.01853 7.81324 2.05453 7.72648C2.09053 7.63972 2.1433 7.56092 2.2098 7.49458L6.49458 3.2098C6.56092 3.1433 6.63973 3.09053 6.72648 3.05453C6.81324 3.01853 6.90625 3 7.00019 3C7.09412 3 7.18713 3.01853 7.27389 3.05453C7.36065 3.09053 7.43946 3.1433 7.50579 3.2098Z"
                                  fill="#14D2B8"/>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M2.71484 8.00014C2.71484 7.81074 2.79008 7.6291 2.92401 7.49517C3.05793 7.36125 3.23957 7.28601 3.42897 7.28601H12.7127C12.9021 7.28601 13.0837 7.36125 13.2176 7.49517C13.3516 7.6291 13.4268 7.81074 13.4268 8.00014C13.4268 8.18954 13.3516 8.37118 13.2176 8.50511C13.0837 8.63903 12.9021 8.71427 12.7127 8.71427H3.42897C3.23957 8.71427 3.05793 8.63903 2.92401 8.50511C2.79008 8.37118 2.71484 8.18954 2.71484 8.00014Z"
                                  fill="#14D2B8"/>
                        </svg>
                    </icon>
                    Back to blog
                </a>
            </div>
        </div>
        <?php
        if (have_posts()) :
            while (have_posts()) : the_post(); ?>
                <section class="pt-8 pt-md-11">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-10 col-lg-9 col-xl-8">
                                <!-- Heading -->
                                <h1 class="display-4 text-left">
                                    <?php the_title(); ?>
                                </h1>
                                <!-- Text -->
                                <p class="lead mb-7 text-left text-muted">
                                    <?php the_author(); ?> | <?php the_time('j M Y'); ?>
                                </p>
                            </div>
                        </div> <!-- / .row -->
                    </div> <!-- / .container -->
                </section>
                <section class="pt-6 pt-md-8">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-10 col-lg-9 col-xl-8">

                                <!-- Fugure -->
                                <figure class="figure mb-7">
                                    <!-- Image -->
                                    <img class="figure-img img-fluid rounded lift lift-lg"
                                         src="<?php the_post_thumbnail_url('large'); ?>" alt="...">
                                </figure>
                                <!-- Text -->
                                <div class="text_container mt-5">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div> <!-- / .row -->
                    </div> <!-- / .container -->
                </section>
                <section class="pagination mt-5 pb-5">
                <!-- Previous and Next Post -->
                <?php if(is_single()) : ?>
                    <!-- Para pantallas grandes -->
                    <div class="container">
                        <div class="d-flex justify-content-between">
                            <div class="link-button">
                                <?php previous_post_link( '%link', 'Previous Post'); ?>
                            </div>
                            <div class="link-button">
                                <?php next_post_link( '%link', 'Next Post'); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <!-- /Previous and Next Post -->
                </section>
            <?php endwhile;
        endif;
        ?>

    </main><!-- #main -->
<?php
get_footer();
