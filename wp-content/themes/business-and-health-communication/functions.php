<?php
/**
 * phaseplay functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package phaseplay
 */

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

if (!function_exists('business_and_health_communication_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function business_and_health_communication_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on phaseplay, use a find and replace
         * to change 'business-and-health-communication' to the name of your theme in all the template files.
         */
        load_theme_textdomain('business-and-health-communication', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(
            array(
                'menu-1' => esc_html__('Primary', 'business-and-health-communication'),
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'style',
                'script',
            )
        );

        // Set up the WordPress core custom background feature.
        add_theme_support(
            'custom-background',
            apply_filters(
                'business_and_health_communication_custom_background_args',
                array(
                    'default-color' => 'ffffff',
                    'default-image' => '',
                )
            )
        );

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support(
            'custom-logo',
            array(
                'height' => 250,
                'width' => 250,
                'flex-width' => true,
                'flex-height' => true,
            )
        );
    }
endif;
add_action('after_setup_theme', 'business_and_health_communication_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function business_and_health_communication_content_width()
{
    $GLOBALS['content_width'] = apply_filters('business_and_health_communication_content_width', 640);
}

add_action('after_setup_theme', 'business_and_health_communication_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function business_and_health_communication_widgets_init()
{
    register_sidebar(
        array(
            'name' => esc_html__('Sidebar', 'business-and-health-communication'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Add widgets here.', 'business-and-health-communication'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
}

add_action('widgets_init', 'business_and_health_communication_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function business_and_health_communication_scripts()
{
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.1', 'all');
    wp_enqueue_style('splide-css', get_template_directory_uri() . '/css/splide.min.css', array(), '1.1', 'all');
    wp_enqueue_style('rangeslider-css', get_template_directory_uri() . '/css/rangeslider.css', array(), '1.1', 'all');
    wp_enqueue_style('aos-css', get_template_directory_uri() . '/css/aos.css', array(), '1.1', 'all');

    wp_enqueue_script('general', get_template_directory_uri() . '/js/general.js', array(), 1.1, true);
    wp_enqueue_script('aos-js', get_template_directory_uri() . '/js/aos.js', array(), 1.1, true);
    wp_enqueue_script('range-slider', get_template_directory_uri() . '/js/rangeslider.min.js', array(), 1.1, true);
    wp_enqueue_script('splide-bundle-js', get_template_directory_uri() . '/js/splide.min.js', array(), 1.1, true);
    wp_enqueue_script('bootstrap-bundle-js', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array(), 1.1, true);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), 1.1, true);
    wp_enqueue_script('firebase-js', get_template_directory_uri() . '/js/custom-firebase.js', array(), 1.1, true);
    wp_enqueue_style('business-and-health-communication-style', get_stylesheet_uri(), array(), _S_VERSION);
    wp_style_add_data('business-and-health-communication-style', 'rtl', 'replace');

    wp_enqueue_script('business-and-health-communication-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'business_and_health_communication_scripts');


// Successfull Add to Cart //
//add_action('woocommerce_before_cart', 'sample', 1);
//
//function sample() {
//    echo "<script type='text/javascript'>console.log('Woocommerce hook works');</script>";
//}

// Successfull Login //

function update_user($customer){
    if ( isset( $_POST['first_name'] ) ) {
        $first_name = $_POST['first_name'];
    }
}

add_action( 'user_register', 'update_user' );

add_filter ( 'woocommerce_account_menu_items', 'misha_remove_my_account_links' );
function misha_remove_my_account_links( $menu_links ){

//    unset( $menu_links['edit-address'] ); // Addresses


    unset( $menu_links['dashboard'] ); // Remove Dashboard
    unset( $menu_links['payment-methods'] ); // Remove Payment Methods
    //unset( $menu_links['orders'] ); // Remove Orders
    unset( $menu_links['downloads'] ); // Disable Downloads
    unset( $menu_links['edit-account'] ); // Remove Account details tab
    //unset( $menu_links['customer-logout'] ); // Remove Logout link

    return $menu_links;

}

// Rename, re-order my account menu items
function fwuk_reorder_my_account_menu() {
    $neworder = array(
        'subscriptions'    => __( 'Subscriptions', 'woocommerce' ),
        'orders'             => __( 'Orders', 'woocommerce' ),
        'edit-address'       => __( 'Addresses', 'woocommerce' ),
        'customer-logout'    => __( 'Logout', 'woocommerce' ),
    );
    return $neworder;
}
add_filter ( 'woocommerce_account_menu_items', 'fwuk_reorder_my_account_menu' );


// ----- Coming Soon Code ------ //
//
//add_action( 'template_redirect', 'themeprefix_coming_soon' );
//function themeprefix_coming_soon() {
//    if( !is_user_logged_in() && !is_page('coming-soon') ){
//        wp_redirect( site_url('coming-soon') );
//        exit();
//    }
//}
/**
 * Edit my account menu order
 */


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if (class_exists('WooCommerce')) {
    require get_template_directory() . '/inc/woocommerce.php';
}


function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


/**
 * Contact Form Add On
 */

add_filter( 'wpcf7_form_elements', 'delicious_wpcf7_form_elements' );

function delicious_wpcf7_form_elements( $form ) {
    $form = do_shortcode( $form );
    return $form;
}

/**
 * Register Custom Menu
 */
function wpb_custom_new_menu() {
    register_nav_menu('my-custom-menu',__( 'My Custom Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );

/**
 * Viewport Fix
 */
add_filter('ocean_meta_viewport', 'owp_child_viewport');
function owp_child_viewport( $viewport ){
    $viewport = '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">';
    return $viewport;
}