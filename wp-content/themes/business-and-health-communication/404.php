<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package phaseplay
 */

get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 not-found">
			<!-- .page-content -->
            <section class="pt-4">
                <div class="blob-container">
                    <svg width="872" height="573" viewBox="0 0 872 573" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0.187108 201.349C-4.52731 118.937 80.7029 53.4189 159.351 -1.31433C232.32 -52.0945 321.479 -76.1466 417.59 -89.1309C542.464 -106.001 680.822 -144.163 783.225 -85.597C893.098 -22.7584 946.517 95.9369 925.496 201.349C906.229 297.963 775.428 343.966 684.266 410.634C598.743 473.178 534.275 578.366 417.59 572.787C301.834 567.252 258.249 454.857 181.542 386.597C113.315 325.884 4.81329 282.219 0.187108 201.349Z" fill="url(#paint0_linear)"></path>
                        <defs>
                            <linearGradient id="paint0_linear" x1="4.11408e-06" y1="228" x2="930" y2="228" gradientUnits="userSpaceOnUse">
                                <stop stop-color="#F2FEFC"></stop>
                                <stop offset="1" stop-color="#E4FFFB"></stop>
                            </linearGradient>
                        </defs>
                    </svg>
                </div>
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-5 col-lg-6 order-md-2 d-flex justify-content-center">

                            <!-- Image -->
                            <img src="/wp-content/uploads/2020/09/404.png" class="img-fluid mw-md-150 mw-lg-130 mb-6 mb-md-0 aos-init aos-animate" alt="..." data-aos="fade-up" data-aos-delay="100">

                        </div>
                        <div class="col-12 col-md-7 col-lg-6 order-md-1 aos-init aos-animate" data-aos="fade-up">

                            <!-- Heading -->
                            <h1 class="display-3 text-center text-md-left">
                                Oops!
                            </h1>

                            <!-- Text -->
                            <p class="lead text-center text-md-left text-muted mb-6 mb-lg-8">
                                We can’t find the page you are looking for.
                            </p>

                            <!-- Buttons -->
                            <div class="text-center text-md-left">
                                <a href="/" class="btn btn-primary shadow lift mr-1 banner-button text-white">
                                    Back to home
                                </a>
                            </div>
                        </div>
                    </div> <!-- / .row -->
                </div> <!-- / .container -->
            </section>
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
