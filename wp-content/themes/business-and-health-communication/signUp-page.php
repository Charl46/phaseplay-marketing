<?php
/*
 * Template Name: Sign Up Page
 * description: >-
  Page template without sidebar
 */
get_header(); ?>
    <div class="container text-center login sign-up" data-layout="container">
        <div class="row flex-center min-vh-100 py-6">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 col-xxl-4 mx-auto">
                <div class="login-text pb-5 pt-4">
                    Sign Up
                </div>
                <div class="card mt-4 mb-5">
                    <div class="top-blob">
                        <img src="/wp-content/uploads/2020/10/login-blob.svg" alt="">
                    </div>
                    <div class="card-body">
                        <div class="row text-left justify-content-between align-items-center mb-2" id="login-header-text">
                            <div class="col-auto">
                                <h5>Have an account? <a href="/login"> Login</a></h5>
                            </div>
                        </div>
                        <?php echo do_shortcode("[firebase_register required_fields='firstName,lastName,phoneNumber' extra_fields='firstName,lastName,phoneNumber' redirect='/my-account/subscriptions/']"); ?>
                        <?php echo do_shortcode("[firebase_error class='your-class-name'][/firebase_error]"); ?>
                        <?php echo do_shortcode("[firebase_show class='your-class-name']You are already logged in...[/firebase_show]"); ?>
                    </div>
                    <div class="bottom-blob">
                        <img src="/wp-content/uploads/2020/09/solution-illustration-1.png" alt="" class="first">
                        <img src="/wp-content/uploads/2020/09/solution-illustration-2.png" alt="" class="second">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>