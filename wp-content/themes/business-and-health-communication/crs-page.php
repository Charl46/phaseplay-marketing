<?php
/*
 * Template Name: CRS Page
 * description: >-
  Page template without sidebar
 */
get_header(); ?>

    <div id="primary" class="site-content">
        <div id="content" role="main">
            <section class="pt-7 pt-md-10 crs">
                <div class="container">
                    <div class="row" data-aos="fade-up" data-aos-delay="100">
                        <div class="col-12 col-md-6">
                            <div class="header-blob">
                                <img src="/wp-content/uploads/2020/09/crs-header-blob.png" alt="">
                            </div>
                            <!-- Heading -->
                            <h3 class="heading mb-0">
                                CSR
                            </h3>

                            <!-- Text -->
                            <p class="text-muted pr-md-5 pt-md-5">
                                Phaseplay recognizes the unique socio-economic challenges in South Africa and values the
                                importance of community development. As such, and as part of the company’s social
                                initiative, free licenses will be made available to identified communities. The
                                following Brand
                                Ambassadors will pioneer the first projects for Phase 1:
                            </p>

                        </div>
                    </div> <!-- / .row -->
                    <div class="row mt-md-12 pt-5 pt-md-0">
                        <div class="col-12 col-md-4 d-flex mr-md-4">

                            <!-- Card -->
                            <div class="card mb-6 shadow-light-lg lift lift-lg" data-aos="zoom-in" data-aos-delay="100">

                                <!-- Image -->
                                <!-- Image -->
                                <img src="/wp-content/uploads/2020/09/cecil.jpg" alt="..." class="card-img-top">

                                <!-- Body -->

                                <!-- Heading -->
                                <h3 class="mt-3">
                                    Cecil Afrika
                                </h3>
                                <p class="mb-3 text-muted">
                                    SAN DIEGO LEGION
                                </p>
                                <!-- Text -->
                                <p class="mb-3 text-muted">
                                    As one of the most decorated Sevens Rugby players, Afrika is internationally
                                    recognised as one of the icons of the game. His illustrious playing career
                                    spanning over more than a decade includes 1462 career points and 66 World Series
                                    tournaments.
                                </p>

                                <p class="mb-0 text-muted">
                                    The former World Rugby Sevens Player of the Year, has turned into a keen business man, as stakeholder in the increasingly popular Eighteen Coffee brand. Cecil recently signed with Major Rugby League team, San Diego Legion in the USA.
                                </p>

                                <div class="achievements pt-5">
                                    <h3 class="">Notable Achievements</h3>
                                    <div class="list">
                                        <ul class="pl-2">
                                            <li>THREE TIME HSBC SEVENS WORLD SERIES CHAMPION</li>
                                            <li>OLYMPIC BRONZE MEDALIST: RIO OLYMPICS</li>
                                            <li>COMMONWEALTH GAMES GOLD MEDALIST</li>
                                            <li>WORLD RUGBY SEVENS PLAYER OF THE YEAR</li>
                                            <li>CAREER TOP POINTS SCORER: SPRINGBOK SEVENS</li>
                                            <li>SEVENS WORLD SERIES TOP TRY AND POINTS SCORER</li>
                                            <li>SPORT ELIZABETH HOMEGROWN HERO OF THE YEAR</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-4 d-flex mt-5 mt-md-0" data-aos="zoom-in" data-aos-delay="100">
                            <div class="crs-blob1">
                                <img src="/wp-content/uploads/2020/09/Rectangle-1-crs.png" alt="">
                            </div>
                            <div class="crs-blob2">
                                <img src="http://localhost:8000/wp-content/uploads/2020/09/Rectangle-2crs.png" alt="">
                            </div>
                            <!-- Card -->
                            <div class="card mb-6 shadow-light-lg lift lift-lg" >
                                <img src="/wp-content/uploads/2020/09/jonathan-png.png" alt="..." class="card-img-top">

                                <!-- Body -->

                                <!-- Heading -->
                                <h3 class="mt-3">
                                    Jonathan Mokuena
                                </h3>

                                <!-- Text -->
                                <p class="mb-3 text-muted">
                                    Varsity Cup Winning Coach
                                </p>
                                <p class="mb-3 text-muted">
                                    Having an accomplished career in both Sevens and Fifteens rugby is no small feat.
                                    Jonathan Mokuena achieved this by representing Griquas at Currie Cup level, the
                                    Lions at Super Rugby level and Springbok Sevens; all with distinction.
                                </p>

                                <p class="mb-0 text-muted">
                                    Mokuena built on this stellar playing career by becoming a distinguished Fifteens
                                    Rugby Coach. Considered as one of the brightest young coaches in South Africa,
                                    Mokuena has won the Varsity Cup with NWU Pukke, as well as reaching the Currie Cup
                                    First Division finals with the Leopards.
                                </p>

                                <div class="achievements pt-5">
                                    <h3 class="">Notable Achievements</h3>
                                    <div class="list">
                                        <ul class="pl-2">
                                            <li>FORMER SPRINGBOK SEVENS CAPTAIN</li>
                                            <li>VARSITY CUP WINNING COACH</li>
                                            <li>FINALIST: CURRIE CUP FIRST DIVISION (HEAD COACH)</li>
                                            <li>MVP VS BRITISH AND IRISH LIONS</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-md-5">
                        <div class="col-12 col-md-4 d-flex mr-md-4 pt-5" data-aos="zoom-in" data-aos-delay="100">
                            <div class="blob-crs-middle">
                                <img src="/wp-content/uploads/2020/09/crs-dotted-orange-blob.png" alt="">
                            </div>
                            <!-- Card -->
                            <div class="card mb-6 shadow-light-lg lift lift-lg">

                                <!-- Image -->
                                <!-- Image -->
                                <img src="/wp-content/uploads/2020/09/Mzwandile.jpg" alt="..." class="card-img-top">

                                <!-- Body -->

                                <!-- Heading -->
                                <h3 class="mt-3">
                                    Mzwandile Stick
                                </h3>

                                <!-- Text -->
                                <p class="mb-3 text-muted">
                                    Springbok Assistant Coach
                                </p>
                                <p class="mb-3 text-muted">
                                    Leadership comes naturally to the Port Elizabeth-born Stick. As captain of the
                                    Springbok Sevens Team, he led the team to their first Sevens World Series title in
                                    2009. An adept playmaker with an excellent understanding of the game made Stick one
                                    of South Africa's greatest Sevens rugby players.
                                </p>

                                <p class="mb-0 text-muted">
                                    That knowledge of the game transferred seamlessly into Fifteens coaching where Stick
                                    won the U19 Currie Cup as Head Coach of his native EP Kings. He subsequently went on
                                    to win a Rugby Championship title as well as a Rugby World Cup as part of the
                                    Springbok Fifteens coaching staff.
                                </p>

                                <div class=>
                                    <h3 class="achievements pt-5">Notable Achievements</h3>
                                    <div class="list">
                                        <ul class="pl-2">
                                            <li>RUGBY WORLD CUP WINNING COACH (ASSISTANT)</li>
                                            <li>RUGBY CHAMPIONSHIP WINNING COACH (ASSISTANT)</li>
                                            <li>U19 CURRIE CUP WINNING COACH (HEAD COACH)</li>
                                            <li>SEVENS WORLD SERIES CHAMPION (PLAYER)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-4 d-flex pt-5" data-aos="zoom-in" data-aos-delay="100">

                            <!-- Card -->
                            <div class="card mb-6 shadow-light-lg lift lift-lg">

                                <!-- Image -->
                                <img src="/wp-content/uploads/2020/09/frankie.jpg" alt="..." class="card-img-top">

                                <!-- Body -->

                                <!-- Heading -->
                                <h3 class="mt-3">
                                    Frankie Horne
                                </h3>

                                <!-- Text -->
                                <p class="mb-3 text-muted">
                                    SA’s Rugby: Head of 7s
                                </p>
                                <p class="mb-3 text-muted">
                                    Frankie Horne, one of the most recognised and accomplished Sevens rugby players in
                                    the world, was a mainstay in the Springbok Sevens Team for numerous seasons. A
                                    combative forward who brought a legacy of hard work to the side.
                                </p>

                                <p class="mb-0 text-muted">
                                    Horne has taken this same work ethic with him off-field, growing the game of Sevens
                                    Rugby through his involvement as Head of Sevens at the SAS. Here Horne focusses on
                                    development and preparation of prospective Sevens Rugby professionals, both locally
                                    and abroad.
                                </p>

                                <div class="achievements pt-5">
                                    <h3 class="">Notable Achievements</h3>
                                    <div class="list">
                                        <ul class="pl-2">
                                            <li>COMMONWEALTH GAMES GOLD MEDALIST</li>
                                            <li>SEVENS WORLD SERIES CHAMPION</li>
                                            <li>FIRST PLAYER TO PLAY 50 CONSECUTIVE TOURNAMENTS</li>
                                            <li>WORLD TENS SERIES CHAMPION (COACH)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-md-5">
                        <div class="col-12 col-md-4 d-flex mr-md-4 pt-5">

                            <!-- Card -->
                            <div class="card mb-6 shadow-light-lg lift lift-lg">

                                <!-- Image -->
                                <img src="/wp-content/uploads/2020/09/vuyo.jpg" alt="..." class="card-img-top">

                                <!-- Body -->

                                <!-- Heading -->
                                <h3 class="mt-3">
                                    Vuyo Zangqa
                                </h3>

                                <!-- Text -->
                                <p class="mb-3 text-muted">
                                    ZASTAVA SEVENS HEAD COACH
                                </p>
                                <p class="mb-3 text-muted">
                                    Vuyo Zangqa was an integral part of the Springbok Sevens Team that won their first
                                    Sevens World Series title. A talented playmaker, his career was unfortunately cut
                                    short by a car accident that affected his sight.
                                </p>

                                <p class="mb-0 text-muted">
                                    However, such is Zangqa's talent and rugby nous that he was able to seamlessly transition into coaching, assisting the then Head Coach, Paul Treu in building a formidable Springbok Sevens Team. Zangqa subsequently moved to Germany where he coached their Sevens and Fifteens Teams with distinction. Zangqa briefly returned to South Africa to take up a coaching position with the Southern Kings Pro-14 Team and was recently appointed as the Head Coach of the Zastava Sevens Team based in St Petersburg, Russia.
                                </p>

                                <div class="achievements pt-5 ">
                                    <h3 class="">Notable Achievements</h3>
                                    <div class="list">
                                        <ul class="pl-2">
                                            <li>RUGBY WORLD CUP WINNING COACH (ASSISTANT)</li>
                                            <li>RUGBY CHAMPIONSHIP WINNING COACH (ASSISTANT)</li>
                                            <li>U19 CURRIE CUP WINNING COACH (HEAD COACH)</li>
                                            <li>SEVENS WORLD SERIES CHAMPION (PLAYER)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-4 d-flex pt-5">
                            <div class="crs-blob-bottom">
                                <img src="/wp-content/uploads/2020/09/crs-blob-bottom.png" alt="">
                            </div>
                            <!-- Card -->
                            <div class="card mb-6 shadow-light-lg lift lift-lg">

                                <!-- Image -->
                                <img src="/wp-content/uploads/2020/09/paul.jpg" alt="..." class="card-img-top">

                                <!-- Body -->

                                <!-- Heading -->
                                <h3 class="mt-3">
                                    Paul Delport
                                </h3>

                                <!-- Text -->
                                <p class="mb-3 text-muted">
                                    SA Women's Sevens Coach
                                </p>
                                <p class="mb-3 text-muted">
                                    Communication is a key factor to success on and off the field. Paul Delport embodies
                                    this. The talented sweeper represented both the Stormers and Springbok Sevens Teams
                                    in his playing days, before venturing into coaching with the SA Rugby Sevens
                                    Academy.
                                </p>

                                <p class="mb-0 text-muted">
                                    Delport's focus then shifted to the SA Women's Sevens Rugby Team where he
                                    established structures such as a centralised squad as well as training in a High
                                    Performance environment. This professional approach resulted in incremental
                                    improvements year-on-year.
                                </p>

                                <div class="achievements pt-5">
                                    <h3 class="">Notable Achievements</h3>
                                    <div class="list">
                                        <ul class="pl-2">
                                            <li>SA WOMEN'S SEVENS HEAD COACH</li>
                                            <li>SA RUGBY ACADEMY ASSISTANT COACH</li>
                                            <li>SEVENS WORLD SERIES CHAMPION</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> <!-- / .row -->
        </div> <!-- / .container -->
        </section>
        <section class="py-6 py-md-8 sponsors pb-md-12 pt-md-12 pt-5">
<!--            <div class="row justify-content-center">-->
<!--                <div class="col-12 col-md-10 col-lg-8 text-center">-->
<!--                    <div class="sponshors_blob">-->
<!--                        <img src="/wp-content/uploads/2020/09/partners-blob.png" alt="">-->
<!--                    </div>-->

<!--                    <h2 class="sponsor-header">-->
<!--                        Our Partners-->
<!--                    </h2>-->
<!---->

<!--                    <p class="font-size-lg text-muted mb-7 mb-md-9 sponsor-text  pt-4 px-4">-->
<!--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br> tempor incididunt ut-->
<!--                        labore et dolore magna aliqua.-->
<!--                    </p>-->
<!---->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="container  mb-md-5">-->
<!--                <div class="row align-items-center justify-content-center">-->
<!--                    <div class="col-6 col-sm-4 col-md-2 mb-4 mb-md-0">-->
<!---->
<!--                        -->
<!--                        <div class="img-fluid text-gray-600 mb-2 mb-md-0 svg-shim"-->
<!--                             data-tooltip="Tooltip appears when hovering over logo to give all details pertaining to the partner and their involvement in PhasePlay.io">-->
<!--                            <img src="/wp-content/uploads/2020/09/crs-check.png" alt="">-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-6 col-sm-4 col-md-2 mb-4 mb-md-0">-->
<!---->
<!--                       -->
<!--                        <div class="img-fluid text-gray-600 mb-2 mb-md-0 svg-shim"-->
<!--                             data-tooltip="Tooltip appears when hovering over logo to give all details pertaining to the partner and their involvement in PhasePlay.io">-->
<!--                            <img src="/wp-content/uploads/2020/09/crs-check.png" alt="">-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-6 col-sm-4 col-md-2 mb-4 mb-md-0">-->
<!---->
<!--                        -->
<!--                        <div class="img-fluid text-gray-600 mb-2 mb-md-0 svg-shim"-->
<!--                             data-tooltip="Tooltip appears when hovering over logo to give all details pertaining to the partner and their involvement in PhasePlay.io">-->
<!--                            <img src="/wp-content/uploads/2020/09/crs-check.png" alt="">-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-6 col-sm-4 col-md-2 mb-4 mb-md-0">-->
<!---->
<!--                        -->
<!--                        <div class="img-fluid text-gray-600 mb-2 mb-md-0 svg-shim"-->
<!--                             data-tooltip="Tooltip appears when hovering over logo to give all details pertaining to the partner and their involvement in PhasePlay.io">-->
<!--                            <img src="/wp-content/uploads/2020/09/crs-check.png" alt="">-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-6 col-sm-4 col-md-2 mb-4 mb-md-0">-->
<!---->
<!--                        -->
<!--                        <div class="img-fluid text-gray-600 mb-2 mb-md-0 svg-shim"-->
<!--                             data-tooltip="Tooltip appears when hovering over logo to give all details pertaining to the partner and their involvement in PhasePlay.io">-->
<!--                            <img src="/wp-content/uploads/2020/09/crs-check.png" alt="">-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                </div> -->
<!--            </div> -->
        </section>
    </div><!-- #content -->
    </div><!-- #primary -->

<?php get_footer(); ?>