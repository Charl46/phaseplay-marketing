<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package phaseplay
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>
    <script type="text/javascript" src="https://www.bugherd.com/sidebarv2.js?apikey=wovvescocsoe3zwqrbxypg"
            async="true"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
    <a class="skip-link screen-reader-text"
       href="#primary"><?php esc_html_e('Skip to content', 'business-and-health-communication'); ?></a>
    <header id="masthead" class="site-header">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="/">
                <img src="/wp-content/uploads/2020/09/site-logo.svg" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="visibility: hidden">
                <ul class="navbar-nav ml-auto">
                    <?php if (is_page(34)) : ?>

                <li class="nav-item active">

                <?php else : ?>

                    <li class="nav-item">

                        <?php endif; ?>
                        <a class="nav-link" href="/pricing">Pricing</a>
                    </li>
                    <?php if (is_page(40) || is_home()) : ?>

                <li class="nav-item dropdown active">

                <?php else : ?>

                    <li class="nav-item dropdown">

                        <?php endif; ?>
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Learn
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php if (is_page(40)) : ?>
                                <a class="dropdown-item active" href="/csr">CSR</a>
                            <?php else : ?>
                                <a class="dropdown-item" href="/csr">CSR</a>
                            <?php endif; ?>
                            <?php if (is_home()) : ?>
                                <a class="dropdown-item active" href="/blog">Blog</a>
                            <?php else : ?>
                                <a class="dropdown-item" href="/blog">Blog</a>
                            <?php endif; ?>
                        </div>
                    </li>
                    <?php
                    global $current_user;
                    get_currentuserinfo();

                    if (user_can($current_user, "subscriber")) { ?>
                        <li class="nav-item user d-flex align-items-center">
                        <span class="icon">
                               <svg width="18" height="18" viewBox="0 0 18 18" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.4167 2.58333V4.41667H11.75V2.58333H15.4167ZM6.25 2.58333V8.08333H2.58333V2.58333H6.25ZM15.4167 9.91667V15.4167H11.75V9.91667H15.4167ZM6.25 13.5833V15.4167H2.58333V13.5833H6.25ZM17.25 0.75H9.91667V6.25H17.25V0.75ZM8.08333 0.75H0.75V9.91667H8.08333V0.75ZM17.25 8.08333H9.91667V17.25H17.25V8.08333ZM8.08333 11.75H0.75V17.25H8.08333V11.75Z"
                                          fill="#CBCFD4"/>
                               </svg>
                            </span>
                            <span id="user-text">
                               <a class="nav-link" href="javascript:void(0)">My Dashboard</a>
                            </span>
                        </li>
                        <?php
                    } ?>
                    <li class="nav-item user" id="not-logged-in">
                        <a class="nav-link" href="/login">
                            <span class="icon">
                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.5 13.125V11.875C12.5 11.212 12.2366 10.5761 11.7678 10.1072C11.2989 9.63839 10.663 9.375 10 9.375H5C4.33696 9.375 3.70107 9.63839 3.23223 10.1072C2.76339 10.5761 2.5 11.212 2.5 11.875V13.125"
                                              stroke="#CBCFD4" stroke-width="2" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                        <path d="M7.5 6.875C8.88071 6.875 10 5.75571 10 4.375C10 2.99429 8.88071 1.875 7.5 1.875C6.11929 1.875 5 2.99429 5 4.375C5 5.75571 6.11929 6.875 7.5 6.875Z"
                                              stroke="#CBCFD4" stroke-width="2" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                </svg>
                            </span>
                            <span id="user-text">
                                Login
                            </span>
                        </a>
                    </li>
                    <li class="nav-item user-drop dropdown" id="logged-in">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                          <span class="icon">
                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12.5 13.125V11.875C12.5 11.212 12.2366 10.5761 11.7678 10.1072C11.2989 9.63839 10.663 9.375 10 9.375H5C4.33696 9.375 3.70107 9.63839 3.23223 10.1072C2.76339 10.5761 2.5 11.212 2.5 11.875V13.125"
                                              stroke="#CBCFD4" stroke-width="2" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                        <path d="M7.5 6.875C8.88071 6.875 10 5.75571 10 4.375C10 2.99429 8.88071 1.875 7.5 1.875C6.11929 1.875 5 2.99429 5 4.375C5 5.75571 6.11929 6.875 7.5 6.875Z"
                                              stroke="#CBCFD4" stroke-width="2" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                </svg>
                            </span>
                            <span id="user-text-logged-in">
                                Login
                            </span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" id="logout-option" href="javascript:void(0)">
                                <?php echo do_shortcode("[firebase_logout redirect='/'][/firebase_logout]"); ?>
                            </a>
                            <a class="dropdown-item" id="logout-option" href="/my-account/subscriptions">
                                My Account
                            </a>
                        </div>
                    </li>
                </ul>
                <div class="div-inline my-2 my-lg-0">
                    <button type="button" id="demo" class="btn btn-primary demo"><a href="/?page_id=100"
                                                                                    style="text-decoration: none">Try
                            the demo</a>
                    </button>
                    <button type="button" id="sign-up-button" class="btn btn-secondary sign-up">
                        <a class="nav-link sign-up" href="/signup" id="navbarDropdownMenuLink">
                            Sign up
                        </a>
                    </button>
                </div>
            </div>
    </header><!-- #masthead -->

    <script type="text/javascript">
        function toggleDropdown (e) {
            const _d = $(e.target).closest('.dropdown'),
                _m = $('.dropdown-menu', _d);
            setTimeout(function(){
                const shouldOpen = e.type !== 'click' && _d.is(':hover');
                _m.toggleClass('show', shouldOpen);
                _d.toggleClass('show', shouldOpen);
                $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
            }, e.type === 'mouseleave' ? 300 : 0);
        }

        $('body')
            .on('mouseenter mouseleave','.dropdown',toggleDropdown)
            .on('click', '.dropdown-menu a', toggleDropdown);
        function preloadFunc() {
            setTimeout(function () {
                document.getElementById("navbarSupportedContent").style.visibility = "visible";
            }, 900);
        }

        preloadFunc();
    </script>