<?php
/*
 * Template Name: Coming Soon Page
 * description: >-
  Page template without sidebar
 */
get_header('special'); ?>
<section class="pt-4 pt-md-12 pb-md-12 coming-soon">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-5 col-lg-6 order-md-2 order-2">
                <!-- Image -->
                <img src="/wp-content/uploads/2020/09/coming-soon-blob.svg" class="img-fluid mw-md-150 mw-lg-130 mb-6 mb-md-0 aos-init aos-animate" alt="..." data-aos="fade-up" data-aos-delay="100">
            </div>
            <div class="col-12 col-md-7 col-lg-6 order-md-1 order-1 aos-init aos-animate" data-aos="fade-up">
                <!-- Heading -->
                <h1 class="display-3 text-center text-md-left">
                    Coming soon
                </h1>
                <!-- Text -->
                <p class="lead text-center text-md-left text-muted mb-6 mb-lg-8">
                    Something exciting is brewing. Check back soon.
                </p>
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</section>
<?php get_footer('special'); ?>
