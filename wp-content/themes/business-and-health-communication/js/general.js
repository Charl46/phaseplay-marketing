(function ($) {
    $(document).ready(function () {
        $('input[type="range"]').rangeslider({
            polyfill: false,
            onInit: function () {
                $('span.ammount').text(200);
                $('#custom-signup').attr("href", `/?add-to-cart=195`);
            },
            onSlide: function (position, value) {
                let slider = $(".rangeslider__handle");
                slider.attr('data-tooltip', value);
                let sliderAmount = slider.attr("data-tooltip");

                if (sliderAmount === '200') {
                    $('#custom-signup').attr("href", `/?add-to-cart=196`);
                } else if (sliderAmount === '300') {
                    $('#custom-signup').attr("href", `/?add-to-cart=197`);
                } else if (sliderAmount === '400') {
                    $('#custom-signup').attr("href", `/?add-to-cart=198`);
                } else if (sliderAmount === '500') {
                    $('#custom-signup').attr("href", `/?add-to-cart=199`);
                } else if (sliderAmount === '600') {
                    $('#custom-signup').attr("href", `/?add-to-cart=200`);
                } else if (sliderAmount === '700') {
                    $('#custom-signup').attr("href", `/?add-to-cart=201`);
                } else if (sliderAmount === '800') {
                    $('#custom-signup').attr("href", `/?add-to-cart=202`);
                } else if (sliderAmount === '900') {
                    $('#custom-signup').attr("href", `/?add-to-cart=203`);
                } else if (sliderAmount === '1000') {
                    $('#custom-signup').attr("href", `/?add-to-cart=204`);
                }
            }
        });
        $('input#price-slider').on('change input', function () {
            if ($(this).val() === '100') {
                $('span.ammount').text('200');
            } else if ($(this).val() === '200') {
                $('span.ammount').text('400');
            } else if ($(this).val() === '300') {
                $('span.ammount').text('570');
            } else if ($(this).val() === '400') {
                $('span.ammount').text('720');
            } else if ($(this).val() === '500') {
                $('span.ammount').text('850');
            } else if ($(this).val() === '600') {
                $('span.ammount').text('960');
            } else if ($(this).val() === '700') {
                $('span.ammount').text('1050');
            } else if ($(this).val() === '800') {
                $('span.ammount').text('1120');
            } else if ($(this).val() === '900') {
                $('span.ammount').text('1170');
            } else if ($(this).val() === '1000') {
                $('span.ammount').text('1200');
            }
            // $('span.ammount').text($(this).val() * 15 / 89,84);
        });
        $(".custom-select").each(function () {
            var classes = $(this).attr("class"),
                id = $(this).attr("id"),
                name = $(this).attr("name");
            var template = '<div class="' + classes + '">';
            template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
            template += '<div class="custom-options">';
            $(this).find("option").each(function () {
                template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
            });
            template += '</div></div>';

            $(this).wrap('<div class="custom-select-wrapper"></div>');
            $(this).hide();
            $(this).after(template);
        });
        $(".custom-option:first-of-type").hover(function () {
            $(this).parents(".custom-options").addClass("option-hover");
        }, function () {
            $(this).parents(".custom-options").removeClass("option-hover");
        });
        $(".custom-select-trigger").on("click", function () {
            $('html').one('click', function () {
                $(".custom-select").removeClass("opened");
            });
            $(this).parents(".custom-select").toggleClass("opened");
            event.stopPropagation();
        });
        $(".custom-option").on("click", function () {
            $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
            $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
            $(this).addClass("selection");
            $(this).parents(".custom-select").removeClass("opened");
            $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
        });
    });
})(jQuery);

$(document).ready(function () {
    var slideThumb = $(".rangeslider__handle");
    slideThumb.attr('data-tooltip', '100');
    $(`:[data-tooltip]`).hover(function () {
        $('.tooltip').remove();
        $(this).css('position', 'relative');
        var $toolTiptext = $(this).attr("data-tooltip");
        $(this).append("<div class='tooltip'><span>test</span>" + $toolTiptext + "</div>");
    }, function () {
        $(this).css('position', '');
        $('.tooltip').remove();
    });

});
$(document).ready(function () {


    //Woocom Template Styling
    $("article header.entry-header").addClass("container");
    $("article header.entry-header").prepend("<div class=\"top-blob\">\n" +
        "                        <img src=\"/wp-content/uploads/2020/10/login-blob.svg\" alt=\"\">\n" +
        "                    </div>");
    $(".woocommerce-MyAccount-navigation-link.woocommerce-MyAccount-navigation-link--dashboard").append("<span class=\"icon\">\n" +
        "  <svg width=\"18\" height=\"20\" viewBox=\"0 0 18 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
        "<path d=\"M9 9C7.93913 9 6.92172 8.57857 6.17157 7.82843C5.42143 7.07828 5 6.06087 5 5C5 3.93913 5.42143 2.92172 6.17157 2.17157C6.92172 1.42143 7.93913 1 9 1C10.0609 1 11.0783 1.42143 11.8284 2.17157C12.5786 2.92172 13 3.93913 13 5C13 6.06087 12.5786 7.07828 11.8284 7.82843C11.0783 8.57857 10.0609 9 9 9Z\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"/>\n" +
        "<path d=\"M1 19V18C1 16.4087 1.63214 14.8826 2.75736 13.7574C3.88258 12.6321 5.4087 12 7 12H11C12.5913 12 14.1174 12.6321 15.2426 13.7574C16.3679 14.8826 17 16.4087 17 18V19\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"/>\n" +
        "</svg>\n" +
        "</span>");
    $(".woocommerce-MyAccount-navigation-link.woocommerce-MyAccount-navigation-link--orders").append("<span class=\"icon\">\n" +
        "  <svg width=\"21\" height=\"21\" viewBox=\"0 0 21 21\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
        "<path fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M0 1.0625C0 0.880164 0.0724328 0.705295 0.201364 0.576364C0.330295 0.447433 0.505164 0.375 0.6875 0.375H2.75C2.90336 0.375042 3.05229 0.426357 3.17313 0.520784C3.29397 0.615211 3.37976 0.747328 3.41688 0.896125L3.97375 3.125H19.9375C20.0385 3.12509 20.1381 3.14742 20.2295 3.19038C20.3208 3.23335 20.4016 3.2959 20.466 3.37361C20.5305 3.45131 20.577 3.54224 20.6024 3.63996C20.6277 3.73768 20.6312 3.83978 20.6126 3.939L18.5501 14.939C18.5206 15.0965 18.437 15.2388 18.3137 15.3413C18.1905 15.4437 18.0353 15.4999 17.875 15.5H5.5C5.33972 15.4999 5.18453 15.4437 5.06125 15.3413C4.93798 15.2388 4.85437 15.0965 4.82488 14.939L2.76375 3.95963L2.21375 1.75H0.6875C0.505164 1.75 0.330295 1.67757 0.201364 1.54864C0.0724328 1.4197 0 1.24484 0 1.0625ZM4.26525 4.5L6.07063 14.125H17.3044L19.1098 4.5H4.26525ZM6.875 15.5C6.14566 15.5 5.44618 15.7897 4.93046 16.3055C4.41473 16.8212 4.125 17.5207 4.125 18.25C4.125 18.9793 4.41473 19.6788 4.93046 20.1945C5.44618 20.7103 6.14566 21 6.875 21C7.60435 21 8.30382 20.7103 8.81954 20.1945C9.33527 19.6788 9.625 18.9793 9.625 18.25C9.625 17.5207 9.33527 16.8212 8.81954 16.3055C8.30382 15.7897 7.60435 15.5 6.875 15.5ZM16.5 15.5C15.7707 15.5 15.0712 15.7897 14.5555 16.3055C14.0397 16.8212 13.75 17.5207 13.75 18.25C13.75 18.9793 14.0397 19.6788 14.5555 20.1945C15.0712 20.7103 15.7707 21 16.5 21C17.2293 21 17.9288 20.7103 18.4445 20.1945C18.9603 19.6788 19.25 18.9793 19.25 18.25C19.25 17.5207 18.9603 16.8212 18.4445 16.3055C17.9288 15.7897 17.2293 15.5 16.5 15.5ZM6.875 16.875C6.51033 16.875 6.16059 17.0199 5.90273 17.2777C5.64487 17.5356 5.5 17.8853 5.5 18.25C5.5 18.6147 5.64487 18.9644 5.90273 19.2223C6.16059 19.4801 6.51033 19.625 6.875 19.625C7.23967 19.625 7.58941 19.4801 7.84727 19.2223C8.10513 18.9644 8.25 18.6147 8.25 18.25C8.25 17.8853 8.10513 17.5356 7.84727 17.2777C7.58941 17.0199 7.23967 16.875 6.875 16.875ZM16.5 16.875C16.1353 16.875 15.7856 17.0199 15.5277 17.2777C15.2699 17.5356 15.125 17.8853 15.125 18.25C15.125 18.6147 15.2699 18.9644 15.5277 19.2223C15.7856 19.4801 16.1353 19.625 16.5 19.625C16.8647 19.625 17.2144 19.4801 17.4723 19.2223C17.7301 18.9644 17.875 18.6147 17.875 18.25C17.875 17.8853 17.7301 17.5356 17.4723 17.2777C17.2144 17.0199 16.8647 16.875 16.5 16.875Z\" fill=\"currentColor\"/>\n" +
        "</svg>\n" +
        "</span>");
    $(".woocommerce-MyAccount-navigation-link.woocommerce-MyAccount-navigation-link--edit-address").append("<span class=\"icon\">\n" +
        "  <svg width=\"19\" height=\"21\" viewBox=\"0 0 19 21\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
        "<path d=\"M6.70039 16.1H6.00039V16.8H6.70039V16.1ZM13.7004 16.1V16.8H14.4004V16.1H13.7004ZM7.40039 16.1V15.4H6.00039V16.1H7.40039ZM13.0004 15.4V16.1H14.4004V15.4H13.0004ZM13.7004 15.4H6.70039V16.8H13.7004V15.4ZM10.2004 12.6C10.943 12.6 11.6552 12.895 12.1803 13.4201C12.7054 13.9452 13.0004 14.6574 13.0004 15.4H14.4004C14.4004 14.2861 13.9579 13.2178 13.1702 12.4302C12.3826 11.6425 11.3143 11.2 10.2004 11.2V12.6ZM7.40039 15.4C7.40039 14.6574 7.69539 13.9452 8.22049 13.4201C8.74559 12.895 9.45778 12.6 10.2004 12.6V11.2C9.08648 11.2 8.01819 11.6425 7.23054 12.4302C6.44289 13.2178 6.00039 14.2861 6.00039 15.4H7.40039ZM10.2004 4.2C9.45778 4.2 8.74559 4.495 8.22049 5.0201C7.69539 5.5452 7.40039 6.25739 7.40039 7H8.80039C8.80039 6.6287 8.94789 6.2726 9.21044 6.01005C9.47299 5.7475 9.82909 5.6 10.2004 5.6V4.2ZM13.0004 7C13.0004 6.25739 12.7054 5.5452 12.1803 5.0201C11.6552 4.495 10.943 4.2 10.2004 4.2V5.6C10.5717 5.6 10.9278 5.7475 11.1903 6.01005C11.4529 6.2726 11.6004 6.6287 11.6004 7H13.0004ZM10.2004 9.8C10.943 9.8 11.6552 9.505 12.1803 8.9799C12.7054 8.4548 13.0004 7.74261 13.0004 7H11.6004C11.6004 7.3713 11.4529 7.7274 11.1903 7.98995C10.9278 8.2525 10.5717 8.4 10.2004 8.4V9.8ZM10.2004 8.4C9.82909 8.4 9.47299 8.2525 9.21044 7.98995C8.94789 7.7274 8.80039 7.3713 8.80039 7H7.40039C7.40039 7.74261 7.69539 8.4548 8.22049 8.9799C8.74559 9.505 9.45778 9.8 10.2004 9.8V8.4ZM3.90039 1.4H16.5004V0H3.90039V1.4ZM17.2004 2.1V18.9H18.6004V2.1H17.2004ZM16.5004 19.6H3.90039V21H16.5004V19.6ZM3.20039 18.9V2.1H1.80039V18.9H3.20039ZM3.90039 19.6C3.71474 19.6 3.53669 19.5263 3.40542 19.395C3.27414 19.2637 3.20039 19.0857 3.20039 18.9H1.80039C1.80039 19.457 2.02164 19.9911 2.41547 20.3849C2.80929 20.7788 3.34344 21 3.90039 21V19.6ZM17.2004 18.9C17.2004 19.0857 17.1266 19.2637 16.9954 19.395C16.8641 19.5263 16.686 19.6 16.5004 19.6V21C17.0573 21 17.5915 20.7788 17.9853 20.3849C18.3791 19.9911 18.6004 19.457 18.6004 18.9H17.2004ZM16.5004 1.4C16.686 1.4 16.8641 1.47375 16.9954 1.60503C17.1266 1.7363 17.2004 1.91435 17.2004 2.1H18.6004C18.6004 1.54305 18.3791 1.0089 17.9853 0.615076C17.5915 0.221249 17.0573 0 16.5004 0V1.4ZM3.90039 0C3.34344 0 2.80929 0.221249 2.41547 0.615076C2.02164 1.0089 1.80039 1.54305 1.80039 2.1H3.20039C3.20039 1.91435 3.27414 1.7363 3.40542 1.60503C3.53669 1.47375 3.71474 1.4 3.90039 1.4V0ZM4.60039 5.6H0.400391V7H4.60039V5.6ZM4.60039 14H0.400391V15.4H4.60039V14Z\" fill=\"currentColor\"/>\n" +
        "</svg>\n" +
        "</span>");
    $(".woocommerce-MyAccount-navigation-link.woocommerce-MyAccount-navigation-link--subscriptions").append("<span class=\"icon\">\n" +
        "  <svg width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
        "<path d=\"M19.875 4.5H4.125C3.08947 4.5 2.25 5.33947 2.25 6.375V17.625C2.25 18.6605 3.08947 19.5 4.125 19.5H19.875C20.9105 19.5 21.75 18.6605 21.75 17.625V6.375C21.75 5.33947 20.9105 4.5 19.875 4.5Z\" stroke=\"currentColor\" stroke-width=\"1.4\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
        "<path d=\"M5.25 7.5L12 12.75L18.75 7.5\" stroke=\"currentColor\" stroke-width=\"1.4\" stroke-linecap=\"round\" stroke-linejoin=\"round\"/>\n" +
        "</svg>\n" +
        "</span>");
    $(".woocommerce-MyAccount-navigation-link.woocommerce-MyAccount-navigation-link--customer-logout").append("<span class=\"icon\">\n" +
        "  <svg width=\"20\" height=\"20\" viewBox=\"0 0 20 20\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
        "<g clip-path=\"url(#clip0)\">\n" +
        "<path d=\"M4.66025 12.8391L2.52804 10.725H13.1055C13.3131 10.725 13.4898 10.6534 13.6273 10.5082C13.7627 10.3653 13.8305 10.1907 13.8305 9.99023C13.8305 9.78972 13.7626 9.61724 13.6254 9.48007C13.4875 9.34217 13.3114 9.275 13.1055 9.275H2.56564L4.66055 7.18009C4.79772 7.04292 4.86562 6.87044 4.86562 6.66992C4.86562 6.4694 4.79772 6.29693 4.66055 6.15976L4.66076 6.15955L4.65492 6.15454C4.55451 6.06847 4.44506 6.00599 4.3267 5.96901C4.20256 5.93021 4.0749 5.93072 3.94639 5.96852C3.8204 6.00557 3.71118 6.06927 3.6207 6.15976L3.62069 6.15975L3.61978 6.16069L-0.0716277 9.94975L-0.139611 10.0195L-0.0716277 10.0893L3.61978 13.8784L3.61923 13.8789L3.62894 13.8867C3.70145 13.9447 3.78117 13.9918 3.86779 14.0279C3.95708 14.0651 4.04823 14.0844 4.14062 14.0844C4.33553 14.0844 4.50887 14.016 4.65674 13.8829L4.65683 13.883L4.66055 13.8793C4.70762 13.8322 4.74636 13.7778 4.77694 13.7166C4.80597 13.6585 4.82818 13.5996 4.84311 13.5399C4.85805 13.4801 4.86562 13.4199 4.86562 13.3594C4.86562 13.2989 4.85805 13.2386 4.84311 13.1789C4.82818 13.1191 4.80597 13.0602 4.77694 13.0022C4.74636 12.941 4.70762 12.8865 4.66055 12.8394L4.66025 12.8391ZM6.775 6.875V6.975H6.875H8.125H8.225V6.875V2.01172C8.225 1.89902 8.25297 1.79198 8.31007 1.68919C8.36658 1.58748 8.44541 1.50559 8.54824 1.44275C8.64903 1.38115 8.76106 1.35 8.88672 1.35H17.9688C18.15 1.35 18.2996 1.41384 18.4238 1.54373C18.5492 1.67475 18.6109 1.82898 18.6109 2.01172V2.01184L18.6305 17.9883C18.6305 17.9883 18.6305 17.9884 18.6305 17.9884C18.6304 18.1711 18.5687 18.3253 18.4434 18.4563C18.3191 18.5862 18.1695 18.65 17.9883 18.65H8.88672C8.70539 18.65 8.55165 18.586 8.42032 18.4547C8.28899 18.3233 8.225 18.1696 8.225 17.9883V13.1055V13.0055H8.125H6.875H6.775V13.1055V18.75C6.775 19.1271 6.90532 19.4491 7.16562 19.7094C7.42592 19.9697 7.74786 20.1 8.125 20.1H18.75C18.8365 20.1 18.9254 20.0892 19.0166 20.0681C19.1047 20.0478 19.1868 20.0239 19.2627 19.9963C19.3419 19.9675 19.42 19.9283 19.497 19.8793C19.5758 19.8292 19.6449 19.7712 19.7036 19.7051C19.7583 19.6436 19.8096 19.5787 19.8573 19.5105C19.9073 19.4391 19.9497 19.3611 19.9849 19.2767C20.0193 19.1942 20.0469 19.1114 20.0677 19.0282C20.0895 18.9411 20.1 18.8483 20.1 18.75V1.25C20.1 0.980737 20.0394 0.742242 19.9143 0.538494C19.7892 0.334797 19.6034 0.171844 19.3621 0.04779L19.3621 0.0477843L19.3611 0.0472761C19.1656 -0.0505015 18.9615 -0.1 18.75 -0.1H8.125C7.74786 -0.1 7.42592 0.0303181 7.16562 0.290617C6.90532 0.550917 6.775 0.872864 6.775 1.25V6.875Z\" fill=\"currentColor\" stroke=\"currentColor\" stroke-width=\"0.2\"/>\n" +
        "</g>\n" +
        "<defs>\n" +
        "<clipPath id=\"clip0\">\n" +
        "<rect width=\"20\" height=\"20\" fill=\"white\"/>\n" +
        "</clipPath>\n" +
        "</defs>\n" +
        "</svg>\n" +
        "</span>");
    $(".woocommerce").addClass("container");
    $(".woocommerce-MyAccount-content").addClass("pt-4");
    $(".shop_table.shop_table_responsive.cart.woocommerce-cart-form__contents").addClass("mt-5");
    $(".woocommerce").addClass("container");

    $(".woocommerce-Addresses").removeClass("col2-set");
    $(".woocommerce-Addresses").addClass("row");
    $(".u-column1").removeClass("col-1");
    $(".u-column1").addClass("col-12 col-md-6");
    $(".u-column2").removeClass("col-1");
    $(".u-column2").addClass("col-12 col-md-6");
    $("a.woocommerce-button.button.pay").addClass("mr-2");
    $("a.woocommerce-button.button.view").addClass("mr-2");
    $("div.woocommerce-address-fields").addClass("mt-4");
    $(".coupon").addClass("mt-5 mb-5");
    $("#coupon_code.input-text").addClass("mr-3");

    $("#customer_details").removeClass("col2-set");
    $("#customer_details").addClass("row");
    $("#customer_details").addClass("mt-4");

    $("div#customer_details div").removeClass("col-1");
    $("div#customer_details div").removeClass("col-2");
    $("div#customer_details div:first-child").addClass("col-md-6 col-12");

    $("a.remove").each(function () {
        $(this).click(function () {
            setTimeout(function () {
                let container = $(".woocommerce");
                if (container.hasClass("container")) {
                } else {
                    container.addClass("container");
                }
            }, 900);
        });
    });

    $(".product-thumbnail a").click(function(e) {
        e.preventDefault();
    });
    $(".product-name a").click(function(e) {
        e.preventDefault();
    });

    var shopButton = $("p.return-to-shop a.button.wc-backward");
    var shopButtonWoocom = $(".woocommerce-order p a");
    shopButtonWoocom.attr("href", '/my-account/subscriptions');
    shopButton.attr("href", '/pricing');
    var subButton = $("p.no_subscriptions.woocommerce-message.woocommerce-message--info.woocommerce-Message.woocommerce-Message--info.woocommerce-info a.woocommerce-Button.button");
    var orderButton = $(".woocommerce-message.woocommerce-message--info.woocommerce-Message.woocommerce-Message--info.woocommerce-info a.woocommerce-Button.button");
    orderButton.attr("href", '/pricing');
    orderButton.text('Pricing Page');
    subButton.attr("href", '/pricing');
    subButton.text('Pricing Page');


    $('a#firebase-login-form__forgot-password').off("click");
    $('a#firebase-login-form__forgot-password').attr("href", '/password');


    //Nav Scroll affect
    window.onscroll = function () {
        myFunction()
    };

    var header = document.getElementById("masthead");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

});