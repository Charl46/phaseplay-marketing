firebase.auth().onAuthStateChanged(user => {
    if (user) {
        //Set User display Name
        document.getElementById("user-text-logged-in").innerText = user.displayName;

        // Remove right buttons
        var myobj = document.getElementById("demo");
        myobj.remove();
        var signUp = document.getElementById("sign-up-button");
        signUp.remove();
        //Logout Button
        var logout = document.getElementById("logout-option");
        logout.onclick = function () {
            firebase.auth().signOut().then(function () {
                window.location.href = '/'; //relative to domain
            }, function (error) {
                // An error happened.
            });
        };

        //hide Login buttons
        document.getElementById("not-logged-in").style.display = "none";
        //hide form headers
        document.getElementById("login-header-text").style.display = "none";
        document.getElementById("sign-up-header-text").style.display = "none";

    } else {
        //hide Login buttons
        document.getElementById("logged-in").style.display = "none";
    }
});
//
// //When user signs up write to firebase
$('#firebase-register-form__submit').click(function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            console.info(user)
        } else {
            firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                    var data = {
                        accountStatus: 'Registered',
                        accountDateCreated: new Date().toISOString().slice(0, 10),
                        accountType: 'Customer',
                        name: user.displayName,
                        email: user.email
                    };

                    var json = JSON.stringify(data);

                    var xhr = new XMLHttpRequest();
                    xhr.open("PATCH", `https://phaseplay-34a20.firebaseio.com/user/${user.uid}.json`);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.send(json);
                } else {
                    // No user is signed in.
                }
            });
        }
    });
});

$('#pro').click(function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            localStorage.setItem('product', 'Pro');
        }
    });
});
//
$('#elite').on("click",function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            localStorage.setItem('product', 'Elite');
        }
    });
});
$('#podium').on("click", function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            localStorage.setItem('product', 'Podium');
        }
    });
});

$('#custom-signup').on("click", function () {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            localStorage.setItem('product', 'Custom');
            localStorage.setItem('users', $(".rangeslider__handle").attr("data-tooltip"));}
    });
});

let page = window.location.pathname;
if(page.includes('/checkout/order-received/')){
    const product = localStorage.getItem('product');
    const users = localStorage.getItem('users');
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            if(localStorage.hasOwnProperty('product')){
                if(product === 'Pro'){
                    var data = {
                        accountStatus: 'Registered',
                        accountDateCreated: new Date().toISOString().slice(0, 10),
                        accountType: 'Pro',
                        name: user.displayName,
                        email: user.email,
                        users: 20
                    };
                    var json = JSON.stringify(data);
        
                    var xhr = new XMLHttpRequest();
                    xhr.open("PATCH", `https://phaseplay-34a20.firebaseio.com/user/${user.uid}.json`);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.send(json);
                }
                if(product === 'Elite'){
                    var data = {
                        accountStatus: 'Registered',
                        accountDateCreated: new Date().toISOString().slice(0, 10),
                        accountType: 'Elite',
                        name: user.displayName,
                        email: user.email,
                        users: 40
                    };
                    var json = JSON.stringify(data);
        
                    var xhr = new XMLHttpRequest();
                    xhr.open("PATCH", `https://phaseplay-34a20.firebaseio.com/user/${user.uid}.json`);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.send(json);
                }
                if(product === 'Podium'){
                    var data = {
                        accountStatus: 'Registered',
                        accountDateCreated: new Date().toISOString().slice(0, 10),
                        accountType: 'Podium',
                        name: user.displayName,
                        email: user.email,
                        users: 80
                    };
                    var json = JSON.stringify(data);
        
                    var xhr = new XMLHttpRequest();
                    xhr.open("PATCH", `https://phaseplay-34a20.firebaseio.com/user/${user.uid}.json`);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.send(json);
                }
                if(product === 'Custom'){
                    var data = {
                        accountStatus: 'Registered',
                        accountDateCreated: new Date().toISOString().slice(0, 10),
                        accountType: 'Enterprise',
                        name: user.displayName,
                        email: user.email,
                        users: users
                    };
                    var json = JSON.stringify(data);
                    var xhr = new XMLHttpRequest();
                    xhr.open("PATCH", `https://phaseplay-34a20.firebaseio.com/user/${user.uid}.json`);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.send(json);
                }
            }
        }
    });
}