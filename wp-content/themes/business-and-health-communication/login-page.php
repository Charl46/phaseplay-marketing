<?php
/*
 * Template Name: Login Page
 * description: >-
  Page template without sidebar
 */
get_header(); ?>
    <div class="container text-center login" data-layout="container">
        <div class="row flex-center min-vh-100 py-6">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 col-xxl-4 mx-auto">
                <div class="login-text pb-5 pt-4">
                    Login
                </div>
                <div class="card mt-4">
                    <div class="top-blob">
                        <img src="/wp-content/uploads/2020/10/login-blob.svg" alt="">
                    </div>
                    <div class="card-body">
                        <div class="row text-left justify-content-between align-items-center mb-2" id="login-header-text">
                            <div class="col-auto">
                                <h5>Don’t have an account? <a href="/sign up"> Sign up</a></h5>
                            </div>
                        </div>
                        <?php echo do_shortcode("[firebase_login button_text='Login' redirect='/my-account/subscriptions/']"); ?>
                        <?php echo do_shortcode("[firebase_error class='your-class-name'][/firebase_error]"); ?>
                        <?php echo do_shortcode("[firebase_show class='your-class-name']You are already logged in...[/firebase_show]"); ?>

                    </div>
                    <div class="bottom-blob">
                        <img src="/wp-content/uploads/2020/09/solution-illustration-1.png" alt="" class="first">
                        <img src="/wp-content/uploads/2020/09/solution-illustration-2.png" alt="" class="second">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>