<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package phaseplay
 */

get_header();
?>

    <div id="primary" class="site-content">
        <div id="content" role="main">
            <section class="slice blog slice-lg bg-section-secondary mt-md-12">
                <div class="container">
                    <div class="mb-5 text-left">
                        <div class="blog-blob">
                            <img src="/wp-content/uploads/2020/09/crs-dotted-orange-blob.png" alt="">
                        </div>
                        <h3 class="header mt-4">Our Blog</h3>
                        <div class="fluid-paragraph mt-3">
                            <p class="lead lh-180">
                                Become a pace setter with our latest insights into the<br> world of high performing
                                teams and athletes.
                            </p>
                        </div>
                    </div>
                    <div class="row position-relative">
                        <div class="blog-blob1">
                            <img src="/wp-content/uploads/2020/09/Rectangle-1-crs.png" alt="">
                        </div>
                        <div class="blog-blob2">
                            <img src="/wp-content/uploads/2020/09/Rectangle-2crs.png" alt="">
                        </div>
                        <?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $args = array(
                            'post_type' => 'post',
                            'order' => 'ASC',
                            'posts_per_page' => 6,
                            'paged' => $paged
                        );
                        // The Query
                        $the_query = new WP_Query ($args);

                        // The Loop
                        while ($the_query->have_posts()) {
                            $the_query->the_post(); ?>
                            <div class="col-lg-4 mt-5 mt-md-3" data-aos="fade-up" data-aos-delay="100">
                                <div class="card hover-shadow-lg hover-translate-y-n10">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="feature-image"
                                             style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
                                        </div>
                                    </a>
                                    <div class="card-body pt-3 text-left">
                                        <h3 class="mb-1"><?php the_title() ?></h3>
                                        <h6 class="text-muted mt-1 mb-3 d-flex align-content-center">
                                            <div class="calendar-icon mr-2">
                                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M18.5 2H16V1.5C16 1.224 15.776 1 15.5 1C15.224 1 15 1.224 15 1.5V2H5V1.5C5 1.224 4.776 1 4.5 1C4.224 1 4 1.224 4 1.5V2H1.5C0.673 2 0 2.673 0 3.5V17.5C0 18.327 0.673 19 1.5 19H18.5C19.327 19 20 18.327 20 17.5V3.5C20 2.673 19.327 2 18.5 2ZM1.5 3H4V4.5C4 4.776 4.224 5 4.5 5C4.776 5 5 4.776 5 4.5V3H15V4.5C15 4.776 15.224 5 15.5 5C15.776 5 16 4.776 16 4.5V3H18.5C18.776 3 19 3.224 19 3.5V6H1V3.5C1 3.224 1.224 3 1.5 3ZM18.5 18H1.5C1.224 18 1 17.776 1 17.5V7H19V17.5C19 17.776 18.776 18 18.5 18Z"
                                                          fill="#5E6166"/>
                                                </svg>
                                            </div>
                                            <?php the_time('j M Y'); ?>
                                        </h6>
                                        <div class="meta-box mb-2">
                                            <?php the_excerpt(); ?>
                                        </div>
                                        <a href="<?php the_permalink() ?>"
                                           class="text-left blog-permalink d-flex align-items-center">
                                            READ MORE
                                            <span class="arrow-icon ml-2 d-flex align-items-center">
                                                <svg width="19" height="13" viewBox="0 0 19 13" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M18.3547 6.14625L12.3547 0.14625C12.1597 -0.04875 11.8427 -0.04875 11.6477 0.14625C11.4527 0.34125 11.4527 0.65825 11.6477 0.85325L16.7937 5.99925H0.500704C0.224705 5.99925 0.000703812 6.22325 0.000703812 6.49925C0.000703812 6.77525 0.224705 6.99925 0.500704 6.99925H16.7937L11.6477 12.1453C11.4527 12.3403 11.4527 12.6572 11.6477 12.8522C11.7457 12.9502 11.8737 12.9982 12.0017 12.9982C12.1297 12.9982 12.2577 12.9492 12.3557 12.8522L18.3557 6.85225C18.5507 6.65725 18.5507 6.34025 18.3557 6.14525L18.3547 6.14625Z"
                                                                  fill="#35725C"/>
                                                </svg>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        /* Restore original Post Data */
                        wp_reset_postdata();

                        ?>
                    </div>
                    <div class="row justify-content-center mt-5 mb-5 mb-md-0">
                        <div class="col-auto">
                            <!-- Submit -->
                            <?php if (previous_posts_link('Newer Posts') || next_posts_link('Older Posts')) : ?>
                                <button type="submit" class="btn btn-primary lift">
                                    <?php previous_posts_link('Newer Posts'); ?>
                                    <?php next_posts_link('Older Posts'); ?>
                                </button>
                            <?php endif;  ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

<?php
get_footer();
