<?php
/*
 * Template Name: Password Reset Page
 * description: >-
  Page template without sidebar
 */
get_header(); ?>
<main id="primary" class="site-main">
    <div class="container text-center login" data-layout="container">
        <div class="row flex-center min-vh-100 py-6">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 col-xxl-4 mx-auto">
                <div class="login-text pb-5 pt-4">
                    Recover password
                </div>
                <div class="card mt-4">
                    <div class="top-blob">
                        <img src="/wp-content/uploads/2020/10/login-blob.svg" alt="">
                    </div>
                    <div class="card-body">
                        <div id="forgotPassword" style="opacity: 1; animation-duration: 400ms;" data-hs-show-animation-target-group="idForm" class="animated slideInUp">
                            <!-- Title -->
                            <div class="text-center mb-7">
                                <p>Instructions will be sent to you.</p>
                            </div>
                            <!-- End Title -->

                            <!-- Input Group -->
                            <div class="js-form-message">
                                <label class="sr-only" for="recoverEmail">Your email</label>
                                <div class="input-group input-group-sm mb-2">
                                    <input type="email" class="form-control" name="email" id="recoverEmail" placeholder="Your email" aria-label="Your email" required="" data-msg="Please enter a valid email address.">
                                </div>
                            </div>
                            <!-- End Input Group -->

                            <div class="mb-3">
                                <button type="submit" class="btn btn-sm btn-primary btn-block">Recover Password</button>
                            </div>

                            <div class="text-center mb-4">
                                <span class="small text-muted">Remember your password?</span>
                                <a class="js-animation-link small font-weight-bold" href="/login">Login
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-blob">
                        <img src="/wp-content/uploads/2020/09/solution-illustration-1.png" alt="" class="first">
                        <img src="/wp-content/uploads/2020/09/solution-illustration-2.png" alt="" class="second">
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
